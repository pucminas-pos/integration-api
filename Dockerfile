FROM maven:3.6.0-jdk-11-slim AS build
COPY src /home/integration-api/src
COPY pom.xml /home/integration-api
RUN mvn -f /home/integration-api/pom.xml clean package

FROM adoptopenjdk/openjdk11:alpine
COPY --from=build /home/integration-api/target/integration-api.jar /usr/local/lib/integration-api.jar
EXPOSE 8082
ENTRYPOINT ["java","-jar","/usr/local/lib/integration-api.jar"]