package br.com.boasaude.integrationapi.infrastructure.api;

import br.com.boasaude.integrationapi.domain.model.external.integrationservice.*;
import br.com.boasaude.integrationapi.domain.model.internal.ExceptionMessage;
import br.com.boasaude.integrationapi.infrastructure.api.pool.IntegrationServicePoolConfig;
import br.com.boasaude.integrationapi.infrastructure.exception.IntegrationServiceException;
import br.com.boasaude.integrationapi.infrastructure.exception.DataDuplicatedException;
import br.com.boasaude.integrationapi.infrastructure.exception.RequestCannotBeModifiedException;
import br.com.boasaude.integrationapi.infrastructure.exception.RequestNotFoundException;
import com.google.gson.Gson;
import feign.Response;
import feign.codec.ErrorDecoder;
import lombok.SneakyThrows;
import org.apache.commons.io.IOUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.*;

import java.io.InputStream;
import java.io.Reader;
import java.io.StringReader;
import java.nio.charset.StandardCharsets;
import java.util.List;

@FeignClient(value = "integrationService-api", url = "${application.integration-service.url}",
        configuration = {IntegrationServiceAPI.IntegrationServiceErrorDecoder.class, IntegrationServicePoolConfig.class})
public interface IntegrationServiceAPI {

    @PostMapping("users")
    User save(@RequestBody User user);

    @GetMapping("users")
    User findByEmail(@RequestParam("email") String email);

    @PostMapping("requests")
    Request save(@RequestBody Request request);

    @PostMapping("replies")
    Reply save(@RequestBody Reply reply);

    @GetMapping("requests/{id}")
    Request findById(@PathVariable("id") Long id);

    @PatchMapping("requests/status/{id}")
    Request changeStatus(@PathVariable("id") Long id, @RequestBody Request request);

    @PutMapping("associates/plan/changing")
    PlanResponse changePlan(@RequestBody PlanRequest planRequest);

    @DeleteMapping("associates/plan/cancelation")
    PlanResponse cancelPlan(@RequestBody PlanRequest planRequest);

    @GetMapping("plans")
    List<Plan> findAllPlan();



    class IntegrationServiceErrorDecoder implements ErrorDecoder {

        @Autowired
        public Gson gson;

        @Override
        public Exception decode(String method, Response response) {
            final HttpStatus status = HttpStatus.valueOf(response.status());

            if (status == HttpStatus.INTERNAL_SERVER_ERROR)
                return new IntegrationServiceException("integration error", HttpStatus.INTERNAL_SERVER_ERROR);

            try {
                Reader reader = response.body().asReader(StandardCharsets.UTF_8);
                ExceptionMessage exceptionMessage = gson.fromJson(reader, ExceptionMessage.class);
                return new IntegrationServiceException(exceptionMessage.getMessage(), status);

            } catch (Exception ex) {
                return new IntegrationServiceException("integration error", HttpStatus.INTERNAL_SERVER_ERROR);
            }
        }
    }
}