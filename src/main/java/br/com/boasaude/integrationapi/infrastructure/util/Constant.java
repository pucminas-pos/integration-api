package br.com.boasaude.integrationapi.infrastructure.util;

public class Constant {

    private Constant() {}

    public static final String X_USER_ID_HEADER = "X-User-Id";
    public static final String X_USER_ROLE_HEADER = "X-User-Role";
}
