package br.com.boasaude.integrationapi.infrastructure.exception;

import org.springframework.http.HttpStatus;

public class IntegrationServiceException extends IntegrationException {

    public IntegrationServiceException(String message, HttpStatus httpStatus) {
        super(message, httpStatus);
    }
}
