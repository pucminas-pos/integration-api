package br.com.boasaude.integrationapi.application.controller;

import br.com.boasaude.integrationapi.domain.model.external.integrationservice.PlanRequest;
import br.com.boasaude.integrationapi.domain.service.AssociateService;
import static br.com.boasaude.integrationapi.infrastructure.util.Constant.X_USER_ID_HEADER;
import lombok.RequiredArgsConstructor;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

@RestController
@RequiredArgsConstructor
@RequestMapping("associates")
public class AssociateController {

    private final AssociateService associateService;


    @PutMapping("plan/changing")
    public ResponseEntity changePlan(@RequestHeader(X_USER_ID_HEADER) String email, @RequestBody PlanRequest planRequest) {
        return ResponseEntity.ok(associateService.requestChangePlan(email, planRequest));
    }

    @DeleteMapping("plan/cancelation")
    public ResponseEntity cancelPlan(@RequestHeader(X_USER_ID_HEADER) String email) {
        return ResponseEntity.ok(associateService.requestCancelPlan(email));
    }
}
