package br.com.boasaude.integrationapi.application.controller;

import br.com.boasaude.integrationapi.domain.model.external.integrationservice.User;
import br.com.boasaude.integrationapi.domain.service.UserService;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

@Slf4j
@RestController
@RequiredArgsConstructor
@RequestMapping("users")
public class UserController {

    private final UserService userService;


    @PostMapping
    public ResponseEntity save(@RequestBody User user) {
        User userPersisted = userService.save(user);
        return ResponseEntity.status(HttpStatus.CREATED).body(userPersisted);
    }

    @GetMapping
    public ResponseEntity findByEmail(@RequestParam("email") String email) {
        return ResponseEntity.ok(userService.findByEmail(email));
    }
}
