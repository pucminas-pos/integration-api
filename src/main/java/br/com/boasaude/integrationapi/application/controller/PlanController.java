package br.com.boasaude.integrationapi.application.controller;

import br.com.boasaude.integrationapi.domain.service.PlanService;
import lombok.RequiredArgsConstructor;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequiredArgsConstructor
@RequestMapping("plans")
public class PlanController {

    private final PlanService planService;


    @GetMapping
    public ResponseEntity findAll() {
        return ResponseEntity.ok(planService.findAll());
    }
}
