package br.com.boasaude.integrationapi.application.controller;

import br.com.boasaude.integrationapi.domain.model.external.integrationservice.Request;
import br.com.boasaude.integrationapi.domain.service.RequestService;
import lombok.RequiredArgsConstructor;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

@RestController
@RequestMapping("requests")
@RequiredArgsConstructor
public class RequestController {

    private final RequestService requestService;


    @PostMapping
    public ResponseEntity save(@RequestBody Request request) {
        return ResponseEntity.status(HttpStatus.CREATED).body(requestService.save(request));
    }

    @GetMapping("{id}")
    public ResponseEntity findById(@PathVariable("id") Long id) {
        return ResponseEntity.ok(requestService.findById(id));
    }

    @PatchMapping("status/{id}")
    public ResponseEntity changeStatus(@PathVariable("id") Long id, @RequestBody Request request) {
        return ResponseEntity.ok(requestService.changeStatus(id, request));
    }
}