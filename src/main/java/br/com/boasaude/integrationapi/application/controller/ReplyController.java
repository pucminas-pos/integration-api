package br.com.boasaude.integrationapi.application.controller;

import br.com.boasaude.integrationapi.domain.model.external.integrationservice.Reply;
import br.com.boasaude.integrationapi.domain.model.external.integrationservice.UserRole;
import br.com.boasaude.integrationapi.domain.service.ReplyService;
import lombok.RequiredArgsConstructor;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import static br.com.boasaude.integrationapi.infrastructure.util.Constant.X_USER_ID_HEADER;
import static br.com.boasaude.integrationapi.infrastructure.util.Constant.X_USER_ROLE_HEADER;

@RestController
@RequestMapping("replies")
@RequiredArgsConstructor
public class ReplyController {

    private final ReplyService replyService;


    @PostMapping
    public ResponseEntity save(@RequestHeader(X_USER_ID_HEADER) String email, @RequestHeader(X_USER_ROLE_HEADER)UserRole role,
                               @RequestBody Reply reply) {
        return ResponseEntity.status(HttpStatus.CREATED).body(replyService.save(reply, email, role));
    }

}