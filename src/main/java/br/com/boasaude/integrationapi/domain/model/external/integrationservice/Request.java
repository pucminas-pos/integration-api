package br.com.boasaude.integrationapi.domain.model.external.integrationservice;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonInclude;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.time.LocalDateTime;
import java.util.List;

@Data
@Builder
@NoArgsConstructor
@AllArgsConstructor
@JsonIgnoreProperties(ignoreUnknown = true)
@JsonInclude(JsonInclude.Include.NON_NULL)
public class Request {

    private Long id;
    private String fullname;
    private String cpf;
    private String email;
    private String role;
    private String subject;
    private String message;
    private RequestType type;
    private String protocol;
    private LocalDateTime createdAt;
    private RequestStatus status;
    private List<Reply> replies;
}
