package br.com.boasaude.integrationapi.domain.model.external.integrationservice;

public enum UserRole {

    ADMIN, ASSOCIATE
}
