package br.com.boasaude.integrationapi.domain.model.external.integrationservice;

public enum PlanCategory {

    BASIC, INTERMEDIATE, VIP

}
