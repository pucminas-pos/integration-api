package br.com.boasaude.integrationapi.domain.model.external.integrationservice;

public enum RequestStatus {

    OPENED, IN_PROGRESS, CLOSED;

}
