package br.com.boasaude.integrationapi.domain.service.impl;

import br.com.boasaude.integrationapi.domain.model.external.integrationservice.User;
import br.com.boasaude.integrationapi.domain.model.external.integrationservice.UserRole;
import br.com.boasaude.integrationapi.domain.service.UserService;
import br.com.boasaude.integrationapi.infrastructure.api.IntegrationServiceAPI;
import br.com.boasaude.integrationapi.infrastructure.exception.UserNotAuthorizedException;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Service;

@Slf4j
@Service
@RequiredArgsConstructor
public class UserServiceImpl implements UserService {

    private final IntegrationServiceAPI integrationServiceAPI;


    @Override
    public User save(User user) {
        if (user.getRole() != UserRole.ASSOCIATE)
            throw new UserNotAuthorizedException("user not authorized");

        return integrationServiceAPI.save(user);
    }

    @Override
    public User findByEmail(String email) { return integrationServiceAPI.findByEmail(email); }
}
