package br.com.boasaude.integrationapi.domain.service;

import br.com.boasaude.integrationapi.domain.model.external.integrationservice.Request;
import br.com.boasaude.integrationapi.domain.model.external.integrationservice.Response;

public interface RequestService {

    Request save(Request request);

    Request changeStatus(Long id, Request request);

    Request findById(Long id);

}
