package br.com.boasaude.integrationapi.domain.service.impl;

import br.com.boasaude.integrationapi.domain.model.external.integrationservice.PlanRequest;
import br.com.boasaude.integrationapi.domain.model.external.integrationservice.PlanResponse;
import br.com.boasaude.integrationapi.domain.service.AssociateService;
import br.com.boasaude.integrationapi.infrastructure.api.IntegrationServiceAPI;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Service;

@Slf4j
@Service
@RequiredArgsConstructor
public class AssociateServiceImpl implements AssociateService {

    private final IntegrationServiceAPI integrationServiceAPI;

    @Override
    public PlanResponse requestChangePlan(String email, PlanRequest planRequest) {
        planRequest.setEmail(email);
        return integrationServiceAPI.changePlan(planRequest);
    }

    @Override
    public PlanResponse requestCancelPlan(String email) {
        PlanRequest planRequest = PlanRequest.builder().email(email).build();
        return integrationServiceAPI.cancelPlan(planRequest);
    }
}
