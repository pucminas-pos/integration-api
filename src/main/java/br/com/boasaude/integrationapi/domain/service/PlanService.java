package br.com.boasaude.integrationapi.domain.service;

import br.com.boasaude.integrationapi.domain.model.external.integrationservice.Plan;

import java.util.List;

public interface PlanService {

    List<Plan> findAll();
}
