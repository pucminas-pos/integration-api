package br.com.boasaude.integrationapi.domain.service.impl;

import br.com.boasaude.integrationapi.domain.model.external.integrationservice.Plan;
import br.com.boasaude.integrationapi.domain.service.PlanService;
import br.com.boasaude.integrationapi.infrastructure.api.IntegrationServiceAPI;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Service;

import java.util.List;

@Slf4j
@Service
@RequiredArgsConstructor
public class PlanServiceImpl implements PlanService {

    private final IntegrationServiceAPI integrationServiceAPI;

    @Override
    public List<Plan> findAll() {
        return integrationServiceAPI.findAllPlan();
    }
}
