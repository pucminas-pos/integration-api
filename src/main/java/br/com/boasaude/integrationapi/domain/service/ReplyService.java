package br.com.boasaude.integrationapi.domain.service;

import br.com.boasaude.integrationapi.domain.model.external.integrationservice.Reply;
import br.com.boasaude.integrationapi.domain.model.external.integrationservice.UserRole;

public interface ReplyService {

    Reply save(Reply reply, String email, UserRole role);

}
