package br.com.boasaude.integrationapi.domain.service;

import br.com.boasaude.integrationapi.domain.model.external.integrationservice.User;

public interface UserService {

    User save(User user);

    User findByEmail(String email);
}
