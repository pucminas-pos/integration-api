package br.com.boasaude.integrationapi.domain.model.external.integrationservice;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonInclude;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.time.LocalDateTime;

@Data
@Builder
@NoArgsConstructor
@AllArgsConstructor
@JsonIgnoreProperties(ignoreUnknown = true)
@JsonInclude(JsonInclude.Include.NON_NULL)
public class Reply {

    private Long id;
    private String fullname;
    private String email;
    private String role;
    private String message;
    private String protocol;
    private LocalDateTime createdAt;
    private Request request;
}
