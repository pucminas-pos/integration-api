package br.com.boasaude.integrationapi.domain.model.external.integrationservice;

public enum Status {

    ACTIVE, SUSPENDED, INACTIVE

}
