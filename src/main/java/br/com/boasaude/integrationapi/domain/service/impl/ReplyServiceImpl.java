package br.com.boasaude.integrationapi.domain.service.impl;

import br.com.boasaude.integrationapi.domain.model.external.integrationservice.Reply;
import br.com.boasaude.integrationapi.domain.model.external.integrationservice.UserRole;
import br.com.boasaude.integrationapi.domain.service.ReplyService;
import br.com.boasaude.integrationapi.infrastructure.api.IntegrationServiceAPI;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Service;

@Slf4j
@Service
@RequiredArgsConstructor
public class ReplyServiceImpl implements ReplyService {

    private final IntegrationServiceAPI integrationServiceAPI;

    @Override
    public Reply save(Reply reply, String email, UserRole role) {
        reply.setRole(role.name());
        reply.setEmail(email);
        return integrationServiceAPI.save(reply);
    }


}
