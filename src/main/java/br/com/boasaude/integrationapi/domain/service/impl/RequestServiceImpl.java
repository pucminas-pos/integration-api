package br.com.boasaude.integrationapi.domain.service.impl;

import br.com.boasaude.integrationapi.domain.model.external.integrationservice.Request;
import br.com.boasaude.integrationapi.domain.model.external.integrationservice.Response;
import br.com.boasaude.integrationapi.domain.service.RequestService;
import br.com.boasaude.integrationapi.infrastructure.api.IntegrationServiceAPI;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Service;

@Slf4j
@Service
@RequiredArgsConstructor
public class RequestServiceImpl implements RequestService {

    private final IntegrationServiceAPI integrationServiceAPI;


    @Override
    public Request save(Request request) {
        return integrationServiceAPI.save(request);
    }

    @Override
    public Request changeStatus(Long id, Request request) {
        return integrationServiceAPI.changeStatus(id, request);
    }

    @Override
    public Request findById(Long id) {
        return integrationServiceAPI.findById(id);
    }

}
