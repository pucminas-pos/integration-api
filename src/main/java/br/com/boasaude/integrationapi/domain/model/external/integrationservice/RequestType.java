package br.com.boasaude.integrationapi.domain.model.external.integrationservice;

public enum RequestType {

    COMPLAINT, SUGGESTION, COMPLIMENT;


}
