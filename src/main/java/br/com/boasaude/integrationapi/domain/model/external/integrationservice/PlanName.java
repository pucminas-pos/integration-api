package br.com.boasaude.integrationapi.domain.model.external.integrationservice;

public enum PlanName {

    INDIVIDUAL, CORPORATE;

}
