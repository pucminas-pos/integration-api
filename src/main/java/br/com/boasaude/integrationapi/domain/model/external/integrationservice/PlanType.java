package br.com.boasaude.integrationapi.domain.model.external.integrationservice;

public enum PlanType {

    MEDICAL, MEDICAL_ODONTOLOGY

}
