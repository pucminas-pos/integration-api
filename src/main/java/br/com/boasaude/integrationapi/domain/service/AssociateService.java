package br.com.boasaude.integrationapi.domain.service;

import br.com.boasaude.integrationapi.domain.model.external.integrationservice.PlanRequest;
import br.com.boasaude.integrationapi.domain.model.external.integrationservice.PlanResponse;

public interface AssociateService {

    PlanResponse requestChangePlan(String email, PlanRequest planRequest);

    PlanResponse requestCancelPlan(String email);
}
