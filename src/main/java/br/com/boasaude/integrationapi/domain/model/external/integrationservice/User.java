package br.com.boasaude.integrationapi.domain.model.external.integrationservice;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonSubTypes;
import com.fasterxml.jackson.annotation.JsonTypeInfo;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.experimental.SuperBuilder;

import javax.validation.constraints.Email;
import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;

@Data
@SuperBuilder
@NoArgsConstructor
@AllArgsConstructor
@JsonIgnoreProperties(ignoreUnknown = true)
@JsonTypeInfo(use = JsonTypeInfo.Id.NAME, property = "role", visible = true)
@JsonSubTypes({
        @JsonSubTypes.Type(value = Associate.class, name = "ASSOCIATE"),
        @JsonSubTypes.Type(value = User.class, name = "ADMIN")
})
@JsonInclude(JsonInclude.Include.NON_NULL)
public class User {

    @NotBlank
    private String fullname;

    @Email
    private String email;

    @NotBlank
    private String password;

    @NotBlank
    private String rg;

    @NotBlank
    private String cpf;

    @NotBlank
    private String telephone;

    @NotBlank
    private String birthdate;

    @NotNull
    private UserRole role;

}
